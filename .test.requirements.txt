# This is a pip requirements.txt file for running bridgedb's tests.
#
# To run install these dependencies and run the tests, do:
#
#     $ pip install -r .test.requirements.txt
#     $ make coverage
#
coverage==5.5
mechanize==0.4.5
pycodestyle==2.7.0
pylint==2.8.2
sure==1.4.11
